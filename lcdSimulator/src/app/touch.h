/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   LCDSIM.h
 * Author: yy
 *
 * Created on 2016��11��21��, ����11:07
 */

#ifndef __TOUCH_H__
#define __TOUCH_H__

#ifdef __cplusplus
extern "C" {
#endif
    
#include "type.h"

void touch_pressed(int x, int y);
void touch_release(void);
int touch_get_screen_xy(int *x, int *y);

#ifdef __cplusplus
}
#endif

#endif /* LCDSIM_H */

