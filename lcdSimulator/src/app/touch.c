#include <windows.h>
#include <memory.h>
#include <math.h>
#include <pthread.h>
#include "type.h"
#include "lcdsim.h"

static int touch_x, touch_y, touch_pen_pressed = FALSE;

void touch_pressed(int x, int y)
{
	touch_x = x;
	touch_y = y;
	touch_pen_pressed = TRUE;
}

void touch_release(void)
{
	touch_pen_pressed = FALSE;
}

int touch_get_screen_xy(int *x, int *y)
{
	if (TRUE == touch_pen_pressed)
	{
		*x = touch_x;
		*y = touch_y;

		return(TRUE);
	}
	else
	{
		return(FALSE);
	}
}



