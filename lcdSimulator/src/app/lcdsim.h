/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   LCDSIM.h
 * Author: yy
 *
 * Created on 2016��11��21��, ����11:07
 */

#ifndef LCDSIM_H
#define LCDSIM_H

#ifdef __cplusplus
extern "C" {
#endif
    
#include "type.h"

#define LCD_WIDTH 800
#define LCD_HEIGHT 480

#define LCD_USE_TRIPLE_BUFFER

typedef union
{
    uint16 buff[LCD_HEIGHT * LCD_WIDTH];
    uint16 buff_xy[LCD_HEIGHT][LCD_WIDTH];	
}frame_buff_rgb565;

typedef union
{
	uint32 buff[LCD_HEIGHT * LCD_WIDTH];
	uint32 buff_xy[LCD_HEIGHT][LCD_WIDTH];
}frame_buff_rgb888;

extern frame_buff_rgb565 frameBuffer[3];
extern void lcd_sim_init(int argc, char **argv);
extern void LCDSIM_SetPixelIndex(int x, int y, int Index, int LayerIndex) ;

#ifdef __cplusplus
}
#endif

#endif /* LCDSIM_H */

