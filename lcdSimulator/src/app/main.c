#include <windows.h>
#include <pthread.h>
#include <stdio.h>
#include <GL/glut.h>
#include <GL/gl.h>
#include "type.h"
#include "lcdsim.h"

#include "tkc\platform.h"
extern int gui_app_start(int lcd_w, int lcd_h);

void* lcd_thread(void *arg)
{
    Sleep(1);

	gui_app_start(LCD_WIDTH, LCD_HEIGHT);

    while (1)
    {
        usleep(1000);

    }
}

int main(int argc, char** argv) 
{
    pthread_t tid;
    
    pthread_create(&tid, NULL, lcd_thread, NULL);
    lcd_sim_init(argc,argv);

    return 0;
}