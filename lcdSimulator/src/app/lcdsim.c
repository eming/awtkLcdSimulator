#include <windows.h>
#include <memory.h>
#include <math.h>
#include <pthread.h>
#include "type.h"
#include "lcdsim.h"
#include <GL/glut.h>
#include <GL/glext.h>
#include "touch.h"

frame_buff_rgb565 frameBuffer[3];
static long int num=0;

#define RGB565_BLUE     0xf800
#define RGB565_GREEN    0x07e0
#define RGB565_RED      0x001f

unsigned int bgr565_to_rgb888(unsigned short n565Color)
{
	unsigned int n888Color = 0;

	// 获取BGR单色，并填充低位
	unsigned char cBlue = (n565Color & RGB565_BLUE) >> 8;
	unsigned char cGreen = (n565Color & RGB565_GREEN) >> 3;
	unsigned char cRed = (n565Color & RGB565_RED) << 3;

	//连接
	n888Color = (cRed << 16) + (cGreen << 8) + (cBlue << 0);
	return n888Color;
}

void mouse_click(int button, int state, int x, int y)
{
    //printf("mouse:%d,%d\r\n",x,y);
    switch(button)
    {
    case GLUT_LEFT_BUTTON:
        switch (state)
        {
        case GLUT_DOWN:
			touch_pressed(x, y);
            break;

        case GLUT_UP:
			touch_release();
            break;
        }
    break;
    
    case GLUT_RIGHT_BUTTON:
        break;
        
    case GLUT_MIDDLE_BUTTON:
        break;
    }
}

void mouse_move(int x, int y)
{
    //printf("mouse:%d,%d\r\n", x, y);
	touch_pressed(x, y);
}

extern void *lcd_fdc_callback(void);
void display(void)
{
	frame_buff_rgb888 fb;
	frame_buff_rgb565 *online_fb;
	int i, j;

#ifdef LCD_USE_TRIPLE_BUFFER
	online_fb = (frame_buff_rgb565*)lcd_fdc_callback();
#else
	online_fb = &frameBuffer[0];
#endif

	if (online_fb != NULL)
	{
		for (i = 0; i < LCD_WIDTH; i++)
		{
			for (j = 0; j < LCD_HEIGHT; j++)
			{
				fb.buff_xy[j][i] = bgr565_to_rgb888(online_fb->buff_xy[LCD_HEIGHT - 1 - j][i]);
			}
		}

		glClear(GL_COLOR_BUFFER_BIT);
		//glRectf(-1.0f, -1.0f, 1.0f, 1.0f);
		glDrawPixels(LCD_WIDTH,LCD_HEIGHT, GL_BGRA_EXT, GL_UNSIGNED_BYTE, fb.buff_xy);
		glFlush();
	}

}

void timerProc(int id)
{
    display();
    glutTimerFunc(20,timerProc,1);//需要在函数中再调用一次，才能保证循环
}

void fb_test(int c)
{
   int i, j;
    
   for (i = 0; i < LCD_WIDTH; i++) 
   {
      for (j = 0; j < LCD_HEIGHT; j++)
      {
         frameBuffer[0].buff_xy[j][i] = c;
      }
   }
}

void lcd_sim_init(int argc, char **argv) 
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE);
    glutInitWindowPosition(800, 300);
    glutInitWindowSize(LCD_WIDTH, LCD_HEIGHT);
    glutCreateWindow("OpenGL Test");
    fb_test(0x0000FF);
    glutDisplayFunc(display);
    timerProc(0);
    glutMouseFunc(mouse_click);
    glutMotionFunc(mouse_move);
    glutMainLoop();
}

void LCDSIM_SetPixelIndex(int x, int y, int Index, int LayerIndex)  
{
    y = LCD_HEIGHT - 1 - y;
    frameBuffer[0].buff_xy[y][x] = Index;
}

unsigned int LCDSIM_GetPixelIndex(int x, int y, int LayerIndex)
{
    y = LCD_HEIGHT - 1 - y;
    return(frameBuffer[0].buff_xy[y][x]);
}


