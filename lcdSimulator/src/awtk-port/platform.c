/**
 * File:   platform.c
 * Author: AWTK Develop Team
 * Brief:  platform dependent function of stm32
 *
 * Copyright (c) 2018 - 2018  Guangzhou ZHIYUAN Electronics Co.,Ltd.
 *
 * this program is distributed in the hope that it will be useful,
 * but without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.  see the
 * license file for more details.
 *
 */

/**
 * history:
 * ================================================================
 * 2018-02-17 li xianjing <xianjimli@hotmail.com> created
 *
 */

#include "sys/time.h"
#include "tkc/mem.h"
#include "base/timer.h"
#include <pthread.h>
#include <awtk_config.h>


uint64_t get_time_ms64()
{
	struct timeval time_v = { 0 };
	gettimeofday(&time_v, NULL);
	return (unsigned long long)time_v.tv_sec * 1000 + time_v.tv_usec / 1000;
}

void sleep_ms(uint32_t ms)
{
	usleep(1000 * ms);

}

#ifndef HAS_STD_MALLOC
static uint32_t s_heam_mem[8 * 1024 * 1024];
#endif

ret_t platform_prepare(void)
{
    static bool_t inited = FALSE;

    if (!inited)
    {
        inited = TRUE;
        #ifndef HAS_STD_MALLOC
        tk_mem_init(s_heam_mem, sizeof(s_heam_mem));
        #endif
    }

    return RET_OK;
}
