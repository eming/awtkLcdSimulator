/**
 * File:   main_loop_stm32_raw.c
 * Author: AWTK Develop Team
 * Brief:  main loop for stm32
 *
 * Copyright (c) 2018 - 2018  Guangzhou ZHIYUAN Electronics Co.,Ltd.
 *
 * this program is distributed in the hope that it will be useful,
 * but without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.  see the
 * license file for more details.
 *
 */

/**
 * history:
 * ================================================================
 * 2018-02-17 li xianjing <xianjimli@hotmail.com> created
 *
 */
 
#include <stdint.h>
#include "stdlib.h"
#include "lcdsim.h"

#include "base/g2d.h"
#include "base/idle.h"
#include "base/timer.h"
#include "main_loop/main_loop_simple.h"


//1mS����һ��
uint8_t platform_disaptch_input(main_loop_t *loop)
{
	int x,y;

    if (touch_get_screen_xy(&x, &y))
    {
        main_loop_post_pointer_event(loop, TRUE, x, y);
    }
    else
    {
        main_loop_post_pointer_event(loop, FALSE, x, y);
    }
}

extern lcd_t *win_create_lcd(wh_t w, wh_t h);

lcd_t *platform_create_lcd(wh_t w, wh_t h)
{
    return win_create_lcd(w, h);
}

#include "main_loop/main_loop_raw.inc"
